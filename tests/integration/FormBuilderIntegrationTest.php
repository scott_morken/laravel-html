<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/9/16
 * Time: 11:07 AM
 */
use Mockery as m;
use Smorken\Html\FormBuilder;

class FormBuilderIntegrationTest extends \BaseTestCase
{

    /**
     * @var FormBuilder
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $view = $this->app[\Illuminate\View\Factory::class];
        $url = m::mock('Illuminate\Contracts\Routing\UrlGenerator');
        $url->shouldReceive('current')->andReturn('/');
        $html = new \Smorken\Html\HtmlBuilder($url, $view);
        $token = 'abc123';
        $this->sut = new FormBuilder($html, $url, $view, $token);
    }

    public function testFormBuilderCanCreateHtml()
    {
        $expected = '<form method="POST" action="/" accept-charset="UTF-8"><input name="_token" type="hidden" value="abc123">';
        $html = $this->sut->open();
        $this->assertEquals($expected, $html);
    }
}
