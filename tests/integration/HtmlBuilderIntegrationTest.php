<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/9/16
 * Time: 11:12 AM
 */

class HtmlBuilderIntegrationTest extends \BaseTestCase
{

    /**
     * @var \Smorken\Html\HtmlBuilder
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = $this->app['html'];
    }

    public function testMessagesEmptyWithNoData()
    {
        $m = $this->sut->messages();
        $this->assertEquals('', $m);
    }

    public function testMessagesHtmlWithData()
    {
        session()->flash('success', 'foo bar');
        $m = $this->sut->messages();
        $this->assertContains('foo bar', $m);
    }

    public function testErrorsEmptyWithNoData()
    {
        $m = $this->sut->errors();
        $this->assertEquals('', $m);
    }

    public function testErrorsHtmlWithData()
    {
        session()->flash('errors', new \Illuminate\Support\MessageBag(['foo' => 'bar']));
        $m = $this->sut->errors();
        $this->assertContains('<li>bar</li>', $m);
    }

    public function testAlertsHtmlWithData()
    {
        $mb = new \Illuminate\Support\MessageBag(['foo' => 'bar']);
        $m = $this->sut->alerts($mb);
        $this->assertContains('<div>bar</div>', (string)$m);
    }

    public function testOperations()
    {
        $items = [
            'Name' => ['url' => '...', 'icon' => 'xyz', 'show_text' => false],
        ];
        $m = $this->sut->operations($items);
        $this->assertContains('<span class="xyz"></span>', (string)$m);
    }

    public function testDetailView()
    {
        $m1 = new \stdClass();
        $m1->id = 1;
        $m1->name = 'test';
        $m1->bar = 'foo';
        $m = (string)$this->sut->detailView($m1, [
            'id' => 'ID',
            'name' => 'Name',
            'bar' => [
                'label' => 'Foo Label',
                'value' => function($m) {
                    return $m->bar . ' ' . $m->name;
                }
            ]
        ]);
        $this->assertContains('test', $m);
        $this->assertContains('Name', $m);
        $this->assertContains('foo test', $m);
    }

    public function testTableView()
    {
        $m1 = new \stdClass();
        $m1->id = 1;
        $m1->name = 'test';
        $m1->bar = 'foo';
        $m2 = new \stdClass();
        $m2->id = 2;
        $m2->name = 'test 2';
        $m2->bar = 'foo 2';
        $models = [$m1, $m2];
        $m = (string)$this->sut->tableView($models, [
            'id' => 'ID',
            'name' => 'Name',
            'bar' => [
                'label' => 'Foo Label',
                'value' => function($m) {
                    return $m->bar . ' ' . $m->name;
                }
            ]
        ]);
        $this->assertContains('<th>Foo Label</th>', $m);
        $this->assertContains('foo test', $m);
        $this->assertContains('foo 2 test 2', $m);
    }
}
