<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 10:01 AM
 */

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Html\Builders\Operations;
use Smorken\Html\HtmlBuilder;

class HtmlBuilderTest extends \BaseTestCase
{

    public function testMessages()
    {
        Session::shouldReceive('has')
               ->andReturnUsing(
                   function ($type) {
                       if ($type === 'warning') {
                           return true;
                       }
                       return false;
                   }
               );
        Session::shouldReceive('get')
               ->with('warning')
               ->once()
               ->andReturn('bar');
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->once()
             ->with('message', 'bar')
             ->andReturn($view);
        $view->shouldReceive('with')
             ->once()
             ->with('type', 'warning')
             ->andReturn($view);
        $view->shouldReceive('make')
            ->with('smorken/html::partials.flash')
            ->once()
            ->andReturn($view);
        $view->shouldReceive('render')->once()->andReturn('foo');
        $b = new HtmlBuilder(null, $view);
        $m = $b->messages();
        $this->assertEquals('foo', $m);
    }

    public function testErrors()
    {
        $errors = m::mock('MessageBag');
        $errors->shouldReceive('all')->andReturn(
            [
                'foo' => 'bar',
            ]
        );
        Session::shouldReceive('has')
               ->with('errors')
               ->once()
               ->andReturn(true);
        Session::shouldReceive('get')
               ->with('errors')
               ->once()
               ->andReturn($errors);
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->once()
             ->with('message', '<ul><li>bar</li></ul>')
             ->andReturn($view);
        $view->shouldReceive('with')
             ->once()
             ->with('type', 'danger')
             ->andReturn($view);
        $view->shouldReceive('make')
            ->with('smorken/html::partials.flash')
            ->once()
            ->andReturn($view);
        $view->shouldReceive('render')->once()->andReturn('foo');
        $b = new HtmlBuilder(null, $view);
        $m = $b->errors();
        $this->assertEquals('foo', $m);
    }

    public function testAlerts()
    {
        $alerts = m::mock('Illuminate\Contracts\Support\MessageBag');
        $alerts->shouldReceive('all')->andReturn(
            [
                'foo' => 'bar',
            ]
        );
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->once()
             ->with('messages', $alerts)
             ->andReturn($view);
        $view->shouldReceive('with')
             ->once()
             ->with(
                 'types',
                 [
                     'warning' => 'warning',
                     'success' => 'success',
                     'danger'  => 'danger',
                     'error'   => 'danger',
                     'info'    => 'info',
                 ]
             )
             ->andReturn($view);
        $view->shouldReceive('make')
            ->with('smorken/html::partials.alerts')
            ->once()
            ->andReturn($view);
        $view->shouldReceive('render')->once()->andReturn('foo');
        $b = new HtmlBuilder(null, $view);
        $m = $b->alerts($alerts);
        $this->assertEquals('foo', $m);
    }

    public function testOperationsDefaults()
    {
        $items = [
            'Name' => ['url' => '...', 'icon' => 'xyz', 'show_text' => false],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $items)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', ['nav-pills', 'nav', 'nav-stacked'])
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.operations')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->operations($items);
        $this->assertEquals('foo', $m);
    }

    public function testOperationsNoStacked()
    {
        $items = [
            'Name' => ['url' => '...', 'icon' => 'xyz', 'show_text' => false],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $items)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', ['nav-pills', 'nav'])
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.operations')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->operations($items, Operations::PILLS, false);
        $this->assertEquals('foo', $m);
    }

    public function testOperationsOverrideType()
    {
        $items = [
            'Name' => ['url' => '...', 'icon' => 'xyz', 'show_text' => false],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $items)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', ['nav-tabs', 'nav', 'nav-stacked'])
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.operations')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->operations($items, Operations::TABS);
        $this->assertEquals('foo', $m);
    }

    public function testOperationsAddClass()
    {
        $items = [
            'Name' => ['url' => '...', 'icon' => 'xyz', 'show_text' => false],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $items)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', ['nav-tabs', 'nav', 'nav-stacked', 'test'])
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.operations')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->operations(
            $items,
            Operations::TABS,
            true,
            [
                'class' => 'test',
            ]
        );
        $this->assertEquals('foo', $m);
    }

    public function testTableView()
    {
        list($models, $parsed, $items) = $this->getModels(2, true);
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('paginator', null)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('items', $items)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
            ->with('models', $models)
            ->once()
            ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', '')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
            ->with('row_callback', m::any())
            ->once()
            ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.table_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->tableView($models, $items, [], 'foocontroller@action');
        $this->assertEquals('foo', $m);
    }

    public function testDetailView()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = [
            'id'    => 'ID',
            'name'  => 'Test Label',
            'field' => [
                'label' => 'Some label',
                'value' => 'Some value',
            ],
        ];
        $parsed = [
            'id'    => ['label' => 'ID', 'value' => 1, 'id' => 'id-view', 'raw' => false],
            'name'  => ['label' => 'Test Label', 'value' => 'test', 'id' => 'name-view', 'raw' => false],
            'field' => ['label' => 'Some label', 'value' => 'Some value', 'id' => 'field-view', 'raw' => false,],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', '')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items);
        $this->assertEquals('foo', $m);
    }

    public function testDetailViewWithInvalidProperty()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = ['test' => 'Test Label'];
        $parsed = [
            'test' => [
                'label' => 'Test Label',
                'value' => 'test is undefined',
                'id'    => 'test-view',
                'raw'   => false,
            ],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', '')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items);
        $this->assertEquals('foo', $m);
    }

    public function testDetailViewOverrideId()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = ['name' => 'Test Label'];
        $parsed = [
            'name' => [
                'label' => 'Test Label',
                'value' => 'test',
                'id'    => 'name-view',
                'raw'   => false,
            ],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'foo-id')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', '')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items, ['id' => 'foo-id']);
        $this->assertEquals('foo', $m);
    }

    public function testDetailViewAddClass()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = ['name' => 'Test Label'];
        $parsed = [
            'name' => [
                'label' => 'Test Label',
                'value' => 'test',
                'id'    => 'name-view',
                'raw'   => false,
            ],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', ['fooclass'])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', '')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items, ['class' => 'fooclass']);
        $this->assertEquals('foo', $m);
    }

    public function testDetailViewAddAttribute()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = ['name' => 'Test Label'];
        $parsed = [
            'name' => [
                'label' => 'Test Label',
                'value' => 'test',
                'id'    => 'name-view',
                'raw'   => false,
            ],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', 'data-foo="bar"')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items, ['data-foo' => 'bar']);
        $this->assertEquals('foo', $m);
    }

    public function testDetailViewSetRaw()
    {
        $model = new \stdClass();
        $model->id = 1;
        $model->name = 'test';
        $items = ['name' => ['label' => 'Some Label', 'value' => '<i>Test</i>', 'raw' => true]];
        $parsed = [
            'name' => [
                'label' => 'Some Label',
                'value' => '<i>Test</i>',
                'id'    => 'name-view',
                'raw'   => true,
            ],
        ];
        $view = m::mock(\Illuminate\Contracts\View\Factory::class);
        $view->shouldReceive('with')
             ->with('items', $parsed)
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('id', 'stdClass-view')
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('classes', [])
             ->once()
             ->andReturn($view);
        $view->shouldReceive('with')
             ->with('attributes', 'data-foo="bar"')
             ->once()
             ->andReturn('foo');
        $view->shouldReceive('make')
            ->with('smorken/html::partials.detail_view')
            ->once()
            ->andReturn($view);
        $b = new HtmlBuilder(null, $view);
        $m = $b->detailView($model, $items, ['data-foo' => 'bar']);
        $this->assertEquals('foo', $m);
    }

    protected function getModels($count = 1, $add_actions = false)
    {
        $models = [];
        $parsed = [];
        $items = [
            'id'   => 'ID',
            'name' => 'name',
            'test' => [
                'label' => 'Some Label',
                'value' => '<i>Test</i>',
                'raw'   => true,
            ],
        ];
        if ($add_actions) {
            $items['actions'] = [
                'label' => 'Actions',
                'raw'   => true,
                'value' => '',
            ];
        }
        for ($i = 0; $i < $count; $i++) {
            $m = new \stdClass();
            $m->id = $i;
            $m->name = 'item ' . $i;
            $models[] = $m;
            $p = [
                'id'   => [
                    'label' => 'ID',
                    'value' => $i,
                ],
                'name' => [
                    'label' => 'Name',
                    'value' => 'item ' . $i,
                ],
                'test' => [
                    'label' => 'Some Label',
                    'value' => '<i>Test</i>',
                    'raw'   => true,
                ],
            ];
            if ($add_actions) {
                $p['actions'] = [
                    'label' => 'Actions',
                    'raw'   => true,
                    'value' => '',
                ];
            }
            $parsed[] = $p;
        }
        return [$models, $parsed, $items];
    }
}
