<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/13/15
 * Time: 2:49 PM
 */

use Mockery as m;
use Smorken\Html\FormBuilder;

class FormBuilderTest extends \BaseTestCase
{

    /** @var  FormBuilder
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $v = $this->app['view'];
        $html = m::mock('Smorken\Html\HtmlBuilder');
        $html->makePartial();
        $url = m::mock('Illuminate\Routing\UrlGenerator');
        $url->shouldReceive('current')->andReturn('/');
        $token = 'abc123';
        $this->sut = new FormBuilder($html, $url, $v, $token);
    }

    public function testCheckboxStandardNoMatchNotChecked()
    {
        $model = new \stdClass();
        $model->chk = '1';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('foo', '1', null);
        $expected = '<input name="foo" type="checkbox" value="1">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }

    public function testCheckboxStandardMatchNotChecked()
    {
        $model = new \stdClass();
        $model->chk = '0';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('chk', '1', null);
        $expected = '<input name="chk" type="checkbox" value="1">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }

    public function testCheckboxStandardChecked()
    {
        $model = new \stdClass();
        $model->chk = '1';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('chk', '1', null);
        $expected = '<input checked="checked" name="chk" type="checkbox" value="1">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }

    public function testCheckboxStringNoMatchNotChecked()
    {
        $model = new \stdClass();
        $model->chk = 'N';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('foo', 'Y', null);
        $expected = '<input name="foo" type="checkbox" value="Y">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }

    public function testCheckboxStringMatchNotChecked()
    {
        $model = new \stdClass();
        $model->chk = 'N';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('chk', 'Y', null);
        $expected = '<input name="chk" type="checkbox" value="Y">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }

    public function testCheckboxStringChecked()
    {
        $model = new \stdClass();
        $model->chk = 'Y';
        $this->sut->model($model);
        $chk = $this->sut->checkbox('chk', 'Y', null);
        $expected = '<input checked="checked" name="chk" type="checkbox" value="Y">';
        $this->assertEquals($expected, $chk);
        $this->sut->close();
    }
}
