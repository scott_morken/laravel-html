<?php

abstract class BaseTestCase extends Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            Smorken\Html\HtmlServiceProvider::class,
        ];
    }
}
