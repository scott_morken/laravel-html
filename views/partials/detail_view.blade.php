<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 7:05 AM
 */
/**
 * @var array $items
 * @var string $id
 * @var array $classes
 * @var string $attributes
 */
?>
<dl class="dl-horizontal {{ implode(' ', $classes) }}" id="{{ $id }}" {{ $attributes }}>
    @foreach($items as $field => $data)
        <dt id="{{ $data['id'] }}">{{ $data['label'] }}</dt>
        <dd>
            @if (isset($data['raw']) && $data['raw'])
                {!! $data['value'] or '&nbsp;' !!}
            @else
                {{ $data['value'] or '&nbsp;' }}
            @endif
        </dd>
    @endforeach
</dl>
