@if ($messages && !$messages->isEmpty())
    @foreach($messages->toArray() as $k => $msgarray)
        <?php $type = isset($types[$k]) ? $types[$k] : 'info'; ?>
        <div class="alert alert-{{ $type }} alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @foreach($msgarray as $message)
                <div>{{ $message }}</div>
            @endforeach
        </div>
    @endforeach
@endif
