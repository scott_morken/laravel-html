<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/19/14
 * Time: 3:07 PM
 */
/* @var string $message */
/* @var string $type */
?>
<div class="alert alert-{{ $type }} alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    @if (is_array($message))
        @foreach ($message as $m)
            <div>{{ $m }}</div>
        @endforeach
    @else
        {{ $message }}
    @endif
</div>
