<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/25/14
 * Time: 11:16 AM
 */
/**
 * @var \Illuminate\Pagination\Paginator|null
 * @var array $items
 * @var array $models
 * @var string $id
 * @var array $classes
 * @var string $attributes
 * @var callback $row_callback
 */
?>
<table class="table {{ implode(' ', $classes) }}" id="{{ $id }}" {{ $attributes }}>
    <thead>
    <tr>
        <?php $headers = $row_callback(null, $items); ?>
        @foreach($headers as $header)
            <th>{{ $header['label'] }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <?php $row = $row_callback($model, $items); ?>
            @foreach($row as $field => $data)
                <td>
                    @if (isset($data['raw']) && $data['raw'])
                        {!! $data['value'] or '&nbsp;' !!}
                    @else
                        {{ $data['value'] or '&nbsp;' }}
                    @endif
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
@if (!$models || count($models) === 0)
    <div class="small">No results found.</div>
@endif
@if ($paginator)
    {!! $paginator->render() !!}
@endif
