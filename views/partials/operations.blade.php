<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 4:24 PM
 */
/* @var array $items */
/* @var array $classes */
/* @var bool $show_text */
?>
<ul class="{{ implode(' ', $classes) }}">
    @foreach($items as $item => $data)
        <li>
            <?php $show_text = !isset($data['show_text']) || (isset($data['show_text']) && $data['show_text']);
            ?>
            @if (isset($data['url']))
                <?php
                $opentag = '<a href="' . $data['url'] . '" title="' . $item . '">';
                $closetag = '</a>';
                ?>
            @else
                <?php
                $opentag = '<span>';
                $closetag = '</span>';
                ?>
            @endif

            {!! $opentag !!}
            @if (isset($data['icon']) && $data['icon'])
                <span class="{{ $data['icon'] }}"></span>
            @endif
            @if ($show_text)
                {!! $item !!}
            @endif
            {!! $closetag !!}
        </li>
    @endforeach
</ul>
