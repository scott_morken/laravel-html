<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/19/14
 * Time: 3:00 PM
 */

namespace Smorken\Html;

use Illuminate\Contracts\Support\MessageBag;
use Smorken\Html\Builders\Operations;

class HtmlBuilder extends \Collective\Html\HtmlBuilder
{

    /**
     * Renders flash messages from the template
     * allowed types: warning, success, danger, info
     * @param string $template
     * @return string
     */
    public function messages($template = 'smorken/html::partials.flash')
    {
        $builder = new Builders\Messages($this->view);
        return $builder->create($template);
    }

    public function errors($template = 'smorken/html::partials.flash')
    {
        $builder = new Builders\Messages($this->view);
        return $builder->errors($template);
    }

    public function alerts(MessageBag $messages, $template = 'smorken/html::partials.alerts')
    {
        $builder = new Builders\Messages($this->view);
        return $builder->alerts($messages, $template);
    }

    public function operations(
        array $items,
        $type = Operations::PILLS,
        $stacked = true,
        $additionalOptions = [],
        $template = 'smorken/html::partials.operations'
    ) {
        $builder = new Builders\Operations($this->view);
        return $builder->create($items, $type, $stacked, $additionalOptions, $template);
    }

    public function detailView($model, array $items, $attributes = [], $template = 'smorken/html::partials.detail_view')
    {
        $builder = new Builders\DetailView($this->view);
        return $builder->create($model, $items, $attributes, $template);
    }

    public function tableView(
        $models,
        array $items,
        $attributes = [],
        $opsController = null,
        $template = 'smorken/html::partials.table_view'
    ) {
        $builder = new Builders\TableView($this->view);
        return $builder->create($models, $items, $attributes, $opsController, $template);
    }
}
