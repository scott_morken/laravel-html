<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/25/14
 * Time: 10:56 AM
 */

namespace Smorken\Html\Builders;

class TableView extends ViewAbstract
{

    protected $opsController;

    public function create(
        $models,
        array $items,
        $attributes = [],
        $opsController = null,
        $template = 'smorken/html::partials.table_view'
    ) {
        $this->opsController = $opsController;
        if ($opsController) {
            $items['actions'] = [
                'label' => 'Actions',
                'raw'   => true,
                'value' => '',
            ];
        }
        return $this->createInternal($models, $items, $attributes, $template);
    }

    protected function createInternal($models, array $items, array $attributes, $template)
    {
        $paginator = $this->createPaginator($models);
        if ($models instanceof \stdClass && property_exists($models, 'items')) {
            $models = $models->items;
        }
        $classes = [];
        $classes = array_merge($classes, $this->getClassesFromArray($attributes));
        $id = isset($attributes['id']) ? $attributes['id'] : $this->createPartialId($models) . '-view';
        $clean_attributes = '';
        $strip = ['id', 'class', 'classes'];
        foreach ($attributes as $key => $item) {
            if (!in_array($key, $strip)) {
                $clean_attributes[] = e($key) . '="' . e($item) . '"';
            }
        }
        if ($clean_attributes) {
            $clean_attributes = implode(' ', $clean_attributes);
        }
        $me = $this;
        $rowcallback = function ($model, $items) use ($me) {
            if (isset($items['actions']) && $model) {
                $ops = call_user_func([$me->opsController, 'opsWithId'], $model->getKey(), null, false);
                $style = \Smorken\Html\Builders\Operations::UNSTYLED;
                if (method_exists($me->opsController, 'opsStyle')) {
                    $style = call_user_func([$me->opsController, 'opsStyle']);
                }
                $items['actions']['value'] = \HTML::operations($ops, $style, false);
            }
            return $me->createItemArray($model, $items);
        };
        $output = $this->view->make($template)
                             ->with('paginator', $paginator)
                             ->with('items', $items)
                             ->with('models', $models)
                             ->with('id', $id)
                             ->with('classes', $classes)
                             ->with('attributes', $clean_attributes)
                             ->with('row_callback', $rowcallback);
        return $output;
    }

    protected function createPaginator($models)
    {
        $paginator = null;
        if ($models instanceof \Illuminate\Contracts\Pagination\Paginator) {
            return $models;
        }
        if ($models instanceof \stdClass && property_exists($models, 'page')) {
            $paginator = \Paginator::make($models->items, $models->totalItems, $models->limit);
        }
        return $paginator;
    }

    protected function createPartialId($models)
    {
        if (!$models) {
            return 'none';
        }
        if (!is_array($models)) {
            return class_basename($models);
        } else {
            return class_basename(head($models));
        }
    }
}
