<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 9:13 AM
 */

namespace Smorken\Html\Builders;

class Messages extends BuilderAbstract
{

    public function alerts($messages, $template = 'smorken/html::partials.alerts')
    {
        $types = [
            'warning' => 'warning',
            'success' => 'success',
            'danger'  => 'danger',
            'error'   => 'danger',
            'info'    => 'info',
        ];
        $output = $this->view->make($template)
                             ->with('types', $types)
                             ->with('messages', $messages)
                             ->render();
        return $output;
    }

    public function create($template = 'smorken/html::partials.flash')
    {
        $output = '';
        $types = ['warning', 'success', 'danger', 'info'];
        foreach ($types as $type) {
            if (session()->has($type)) {
                $output .= $this->view->make($template)
                                      ->with('message', $this->escape(session()->get($type)))
                                      ->with('type', $type)
                                      ->render();
            }
        }
        $output .= $this->errors($template);
        return $output;
    }

    public function errors($template = 'smorken/html::partials.flash')
    {
        $output = '';
        if (session()->has('errors')) {
            $errors = session()->get('errors');
            $parsed = $errors->all();
            $output .= $this->view->make($template)
                                  ->with('message', $this->escape($parsed))
                                  ->with('type', 'danger')
                                  ->render();
        }
        return $output;
    }

    protected function escape($value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                if (is_array($v)) {
                    $value[$k] = $this->escape($v);
                } else {
                    $value[$k] = e($v);
                }
            }
        } else {
            $value = e($value);
        }
        return $value;
    }
}
