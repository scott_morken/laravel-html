<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 9:06 AM
 */

namespace Smorken\Html\Builders;

use Illuminate\Contracts\View\Factory;

abstract class BuilderAbstract
{

    /**
     * @var Factory
     */
    protected $view;

    public function __construct(Factory $view)
    {
        $this->view = $view;
    }

    //abstract function create();

    protected function getClassesFromArray($array)
    {
        $classes = [];
        $poss_keys = ['class', 'classes'];
        foreach ($poss_keys as $key) {
            if (isset($array[$key])) {
                if (is_array($array[$key])) {
                    $classes = array_merge($classes, $array[$key]);
                } else {
                    $classes[] = $array[$key];
                }
            }
        }
        return $classes;
    }
}
