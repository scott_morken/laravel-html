<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 9:04 AM
 */

namespace Smorken\Html\Builders;

class DetailView extends ViewAbstract
{

    public function create($model, array $items, $attributes = [], $template = 'smorken/html::partials.detail_view')
    {
        return $this->createInternal($model, $items, $attributes, $template);
    }
}
