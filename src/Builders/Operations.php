<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 9:09 AM
 */

namespace Smorken\Html\Builders;

class Operations extends BuilderAbstract
{

    const PILLS = 'nav-pills';
    const TABS = 'nav-tabs';
    const INLINE = 'nav-inline';
    const UNSTYLED = 'list-unstyled';

    public function create(
        array $items,
        $type = self::PILLS,
        $stacked = true,
        $additionalOptions = [],
        $template = 'smorken/html::partials.operations'
    ) {
        $classes = [
            $type,
        ];
        if (stripos($type, 'nav') === 0) {
            $classes[] = 'nav';
        }
        if ($stacked && stripos($type, 'nav') === 0) {
            $classes[] = 'nav-stacked';
        } else {
            if (!$stacked && stripos($type, 'nav') !== 0) {
                $classes[] = 'list-inline';
            }
        }
        $classes = array_merge($classes, $this->getClassesFromArray($additionalOptions));
        $output = $this->view->make($template)
                             ->with('items', $items)
                             ->with('classes', $classes);

        return $output;
    }
}
