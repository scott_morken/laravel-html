<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/25/14
 * Time: 10:57 AM
 */

namespace Smorken\Html\Builders;

abstract class ViewAbstract extends BuilderAbstract
{

    protected static $not_set_value = '<i>not set</i>';

    public static function setNotSetValue($value)
    {
        static::$not_set_value = $value;
    }

    protected function createInternal($model, array $items, array $attributes, $template)
    {
        $parsed = $this->createItemArray($model, $items);
        $classes = [];
        $classes = array_merge($classes, $this->getClassesFromArray($attributes));
        $id = isset($attributes['id']) ? $attributes['id'] : class_basename($model) . '-view';
        $clean_attributes = '';
        $strip = ['id', 'class', 'classes'];
        foreach ($attributes as $key => $item) {
            if (!in_array($key, $strip)) {
                $clean_attributes[] = e($key) . '="' . e($item) . '"';
            }
        }
        if ($clean_attributes) {
            $clean_attributes = implode(' ', $clean_attributes);
        }
        $output = $this->view->make($template)
                             ->with('items', $parsed)
                             ->with('id', $id)
                             ->with('classes', $classes)
                             ->with('attributes', $clean_attributes);
        return $output;
    }

    public function createItemArray($model, $items)
    {
        $parsed = [];
        foreach ($items as $field => $data) {
            $label = $value = $id = $raw = null;
            $id = $field . '-view';
            if (is_scalar($data)) {
                $value = $this->getPropertyFromObject($model, $field);
                $label = $data;
                $raw = false;
            } else {
                if (is_array($data)) {
                    $label = isset($data['label']) ? $data['label'] : studly_case($field);
                    $value = isset($data['value']) ? $data['value'] : $this->getPropertyFromObject($model, $field);
                    $raw = isset($data['raw']) && $data['raw'];
                }
            }
            if ($label) {
                if ($value === static::$not_set_value) {
                    $raw = true;
                }
                if ($model && $value instanceof \Closure) {
                    $value = $value($model);
                }
                $parsed[$field] = [
                    'label' => $label,
                    'value' => $value,
                    'id'    => $id,
                    'raw'   => $raw,
                ];
            }
        }
        return $parsed;
    }

    protected function getPropertyFromObject($model, $prop)
    {
        try {
            $val = $model ? $model->$prop : '';
            if ($val === '' || $val === null) {
                $val = static::$not_set_value;
            }
            return $val;
        } catch (\Exception $e) {
            return "$prop is undefined";
        }
    }
}
