<?php namespace Smorken\Html;

use Illuminate\Support\ServiceProvider;

class HtmlServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/html');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path() . '/resources/views/vendor/smorken/html',
            ]
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerHtmlBuilder();

        $this->registerFormBuilder();

        $this->app->alias('form', 'Smorken\Html\FormBuilder');
        $this->app->alias('html', 'Smorken\Html\HtmlBuilder');
    }

    protected function registerHtmlBuilder()
    {
        $this->app->singleton(
            'html',
            function ($app) {
                return new HtmlBuilder($app['url'], $app['view']);
            }
        );
    }

    /**
     * Register the form builder instance.
     *
     * @return void
     */
    protected function registerFormBuilder()
    {
        $this->app->singleton(
            'form',
            function ($app) {
                $form = new FormBuilder($app['html'], $app['url'], $app['view'], $app['session.store']->token());
                return $form->setSessionStore($app['session.store']);
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['html', 'form', 'Smorken\Html\HtmlBuilder', 'Smorken\Html\FormBuilder'];
    }
}
