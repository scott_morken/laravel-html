<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/19/14
 * Time: 3:00 PM
 */

namespace Smorken\Html;

class FormBuilder extends \Collective\Html\FormBuilder
{

    /**
     * Get the check state for a checkbox input.
     *
     * @param  string $name
     * @param  mixed $value
     * @param  bool $checked
     * @return bool
     */
    protected function getCheckboxCheckedState($name, $value, $checked)
    {
        if (isset($this->session) && !$this->oldInputIsEmpty() && is_null($this->old($name))) {
            return false;
        }
        if ($this->missingOldAndModel($name)) {
            return $checked;
        }
        $posted = $this->getValueAttribute($name);
        if (is_string($posted) && (bool)$posted) {
            $posted = [$posted];
        }
        return is_array($posted) ? in_array($value, $posted) : (bool)$posted;
    }
}
